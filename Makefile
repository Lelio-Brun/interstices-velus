.PHONY: clean

all: article.pdf article.docx

article.pdf: README.md velus.png euler.png
	@pandoc -o $@ $<

article.docx: README.md velus.png
	@pandoc -o $@ $<

velus.pdf: velus.tex lus.pdf s.pdf
	@pdflatex $<

velus.png: velus.pdf
	@convert -quiet -density 1200 $< -resize 25% -alpha remove -format png $@
	@rm -f $*.aux $*.log $*.nav $*.out $*.snm $*.toc $*.auxlock

euler.pdf: euler.tex eulerscade.pdf
	@pdflatex $<

euler.png: euler.pdf
	@convert -quiet -density 1200 $< -resize 25% -alpha remove -format png $@
	@rm -f $*.aux $*.log $*.nav $*.out $*.snm $*.toc $*.auxlock

clean:
	@rm -f velus.png euler.png article.pdf
